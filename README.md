# GSBaseKit

[![CI Status](https://img.shields.io/travis/jiangergo/GSBaseKit.svg?style=flat)](https://travis-ci.org/jiangergo/GSBaseKit)
[![Version](https://img.shields.io/cocoapods/v/GSBaseKit.svg?style=flat)](https://cocoapods.org/pods/GSBaseKit)
[![License](https://img.shields.io/cocoapods/l/GSBaseKit.svg?style=flat)](https://cocoapods.org/pods/GSBaseKit)
[![Platform](https://img.shields.io/cocoapods/p/GSBaseKit.svg?style=flat)](https://cocoapods.org/pods/GSBaseKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GSBaseKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GSBaseKit'
```

## Author

jiangergo, jiangergo@163.com

## License

GSBaseKit is available under the MIT license. See the LICENSE file for more info.
