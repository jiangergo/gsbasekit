//
//  BaseNavigationController.swift
//  PowerMarket
//
//  Created by Jiangergo on 29/03/2018.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    var isAnimation = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.hidesBackButton = true
        if childViewControllers.count > 0 {
            UINavigationBar.appearance().backItem?.hidesBackButton = false
            
            let backBtn = UIButton(type: .custom)
            backBtn.setImage(UIImage(named: "nav_back"), for: .normal)
            backBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
            backBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            backBtn.contentHorizontalAlignment = .left
            let backBarBtn = UIBarButtonItem(customView: backBtn)
            let spaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            spaceItem.width = -5
            viewController.navigationItem.leftBarButtonItems = [spaceItem, backBarBtn]
            viewController.hidesBottomBarWhenPushed = true
        }
        
        super.pushViewController(viewController, animated: animated)
    }
    
    @objc func backBtnClick() {
        popViewController(animated: isAnimation)
    }

}
