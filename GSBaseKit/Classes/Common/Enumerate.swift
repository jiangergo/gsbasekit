//
//  Enumerate.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/25.
//  Copyright © 2018 gzgs. All rights reserved.
//

import Foundation

enum StatusCode: String {
    case normal = "NORMAL"
    case sesstiontimeout = "SESSIONTIMEOUT"
}

/**
 * 层级分类
 */
enum LevelType: Int {
    // 省
    case province = 0
    // 地
    case land = 1
    // 县
    case county = 2
    // 所
    case place = 3
}

enum DepLevel: Int {
    case province = 2
    case city = 4
    case county = 6
    case place = 8
    
    static func getLevel(depid: String) -> DepLevel {
        switch depid.count {
        case 2:
            return .province
        case 4:
            return .city
        case 6:
            return .county
        default:
            return .place
        }
    }
}
