//
//  SearchView.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/27.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class SearchView: UIView {
    
    var search: ((_ text: String) -> Void)?
    
    fileprivate var isShow: Bool = false
    
    fileprivate lazy var container: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.0 / 255.0, green: 104.0 / 255.0, blue: 183.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    fileprivate lazy var searchIcon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "search_icon")
        view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(show))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    fileprivate lazy var textfield: UITextField = {
        let field = UITextField()
        field.backgroundColor = UIColor.clear
        field.font = UIFont.systemFont(ofSize: 14)
        field.textColor = UIColor.white
        field.attributedPlaceholder = NSAttributedString(string: "搜索", attributes: [.font: UIFont.systemFont(ofSize: 12), .foregroundColor: UIColor.white])
        field.returnKeyType = .done
        field.delegate = self
        return field
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        configureSearch()
        UIView.animate(withDuration: 0.1) {
            self.container.transform = CGAffineTransform(translationX: ScreenWidth - 58 - 2*CommonMargin - 10, y: 0)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: UIViewNoIntrinsicMetric, height: 30)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureSearch() {
        alpha = 0.8
        
        addSubview(container)
        container.snp.makeConstraints { make in
            make.top.bottom.leading.equalToSuperview()
//            make.leading.equalToSuperview().offset(HomeMargin)
            make.trailing.equalToSuperview().offset(-2*CommonMargin)
//            make.edges.equalToSuperview()
        }
        
        container.addSubview(searchIcon)
        container.addSubview(textfield)
        searchIcon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(SubMargin)
            make.width.height.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        textfield.snp.makeConstraints { make in
            make.leading.equalTo(searchIcon.snp.trailing).offset(4)
            make.trailing.equalToSuperview().offset(-SubMargin)
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(2)
        }
    }
    
    @objc func show() {
        if isShow {
            return
        }
        isShow = true
        UIView.animate(withDuration: 0.2, animations: {
            self.container.transform = CGAffineTransform.identity
        }) { (_) in
            self.textfield.becomeFirstResponder()
        }
    }
    
    @objc func hidden() {
        isShow = false
        UIView.animate(withDuration: 0.2, animations: {
            self.container.transform = CGAffineTransform(translationX: ScreenWidth - 58 - 2*CommonMargin - 10, y: 0)
        }) { (_) in
            self.textfield.resignFirstResponder()
        }
    }
    
    @objc fileprivate func searchAction(sender: UIButton) {
        self.textfield.resignFirstResponder()
        guard let keyword = textfield.text else { return }
        if let closure = search {
            closure(keyword)
        }
        hidden()
    }
}

extension SearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard let keyword = textfield.text else { return true }
        if let closure = search {
            closure(keyword)
        }
        hidden()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        show()
        return true
    }
}
