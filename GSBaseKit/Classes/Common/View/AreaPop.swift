//
//  AreaPop.swift
//  StarClass_iOS
//
//  Created by Jiangergo on 2018/9/11.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class AreaLabel: UIView {
    
    var selectedIndex: ((_ rangeID: String) -> ())?

    fileprivate lazy var title: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.text = "全省"
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedPop(sender:)))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    fileprivate lazy var arrow: UIImageView = {
        let arrow = UIImageView()
        arrow.image = UIImage(named: "more_arrow")
        arrow.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedPop(sender:)))
        arrow.addGestureRecognizer(tap)
        return arrow
    }()
    
    fileprivate lazy var popView: AreaPop = {
        let pop = AreaPop()
        return pop
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configurePop()
        popView.didSelected = { [title, weak self] (index, text) in
            title.text = text
            if let closure = self?.selectedIndex {
                var rangeid = "06"
                switch index {
                case 1:
                    rangeid = UserStore.getLoginUserInfo()?.deptId ?? "06"
                default:
                    rangeid = "06"
                }
                closure(rangeid)
            }
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: title.intrinsicContentSize.width + arrow.intrinsicContentSize.width + 10, height: 25)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configurePop() {
        backgroundColor = UIColor.clear
       
        addSubview(title)
        addSubview(arrow)
        
        title.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.trailing.equalTo(arrow.snp.leading).offset(-4)
        }
        
        arrow.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.equalTo(10)
            make.height.equalTo(8)
            make.trailing.equalToSuperview()
        }
    }
    
    @objc fileprivate func tappedPop(sender: UITapGestureRecognizer) {
        popView.show()
    }
}

class AreaPop: UIView {
    
    fileprivate let AreaCellID = "AreaCellID"
    
    fileprivate var areas = ["全省", "区域"]
    
    var didSelected: ((_ index: Int, _ text: String) -> ())?

    fileprivate lazy var shadow: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.alpha = 0.7
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        view.addGestureRecognizer(tap)
        return view
    }()

    fileprivate lazy var popView: UITableView = {
        let view = UITableView(frame: CGRect.zero, style: .plain)
        view.backgroundColor = UIColor.clear
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.separatorInset = UIEdgeInsetsMake(0, 5, 0, 5)
        view.tableFooterView = UIView()
        view.rowHeight = 32
        view.dataSource = self
        view.delegate = self
        view.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: AreaCellID)
        return view
    }()
    
    fileprivate lazy var popBg: UIImageView = {
        let bg = UIImageView()
        bg.image = UIImage(named: "pop_bg")
        return bg
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configurePop()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func configurePop() {
        frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)

        addSubview(shadow)
        addSubview(popBg)
        addSubview(popView)

        shadow.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        popBg.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-HomeMargin)
            make.top.equalToSuperview().offset(NavigationH + 40)
            make.width.equalTo(80)
            make.height.equalTo(72)
        }
        
        popView.snp.makeConstraints { make in
            make.edges.equalTo(popBg)
        }
    }
    
    func show() {
        UIApplication.shared.keyWindow?.addSubview(self)
        self.isHidden = false
    }

    func hidden() {
        self.isHidden = true
        self.removeFromSuperview()
    }

    @objc fileprivate func closeAction() {
        hidden()
    }

}

extension AreaPop: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return areas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AreaCellID, for: indexPath)
        cell.textLabel?.text = areas[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell.textLabel?.textAlignment = .center
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let closure = didSelected {
            closure(indexPath.row, areas[indexPath.row])
        }
        hidden()
    }
}
