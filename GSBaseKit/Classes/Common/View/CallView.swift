//
//  CallView.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/7/5.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class CallView: UIView {
    
    fileprivate lazy var shadow: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.7
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    fileprivate lazy var backView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var closeBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "close_white"), for: .normal)
        btn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var backImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "detail_call_image")
        return view
    }()
    
    lazy var director: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var number: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var callBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("立即拨打", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btn.setBackgroundImage(UIImage(named: "detail_call_button"), for: .normal)
        btn.addTarget(self, action: #selector(callAction(sender:)), for: .touchUpInside)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureView() {
        
        frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
        
        addSubview(shadow)
        addSubview(backView)
        
        shadow.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(235)
            make.height.equalTo(150)
        }
        
        backView.addSubview(backImage)
        backView.addSubview(director)
        backView.addSubview(number)
        backView.addSubview(callBtn)
        backView.addSubview(closeBtn)
        
        backImage.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
        }
        
        director.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(25)
            make.width.equalTo(200)
        }
        
        number.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(director.snp.bottom).offset(3)
        }
        
        callBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        }
        
        closeBtn.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(3)
            make.trailing.equalToSuperview().offset(-6)
        }
    }
    
    func show(director: String, telephone: String) {
        self.director.text = director
        self.number.text = telephone
        if telephone.count == 0 {
            PowerHUD.showInfo(info: "暂无电话")
            return
        }
        UIApplication.shared.keyWindow?.addSubview(self)
        self.isHidden = false
    }
    
    func hidden() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    @objc fileprivate func callAction(sender: UIButton) {
        UIApplication.shared.openURL(URL(string: "tel:" + (number.text ?? ""))!)
    }
    
    @objc fileprivate func closeAction() {
        hidden()
    }
    
}
