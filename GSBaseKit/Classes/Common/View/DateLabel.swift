//
//  DateLabel.swift
//  StarClass_iOS
//
//  Created by Jiangergo on 2018/9/10.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class DateLabel: UIView {
    
    var tappedDate: (() -> Void)?
    
    var isLeft: Bool = true {
        didSet {
            if isLeft {
                dateIcon.snp.remakeConstraints { make in
                    make.leading.equalToSuperview()
                    make.width.height.equalTo(20)
                    make.centerY.equalToSuperview()
                }
                
                label.snp.remakeConstraints { make in
                    make.leading.equalTo(dateIcon.snp.trailing).offset(4)
                    make.centerY.equalTo(dateIcon)
                }
            }
            else {
                dateIcon.snp.remakeConstraints { make in
                    make.trailing.equalToSuperview()
                    make.width.height.equalTo(20)
                    make.centerY.equalToSuperview()
                }
                label.snp.remakeConstraints { make in
                    make.trailing.equalTo(dateIcon.snp.leading).offset(-4)
                    make.centerY.equalTo(dateIcon)
                }
            }
        }
    }
    
    fileprivate lazy var dateIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(named: "calendar_icon")?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = UIColor.white
        return icon
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.white
        return label
    }()
    
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: dateIcon.intrinsicContentSize.width + label.intrinsicContentSize.width + 5, height: 24)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedLabel(sender:)))
        addGestureRecognizer(tap)
        
        configureLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureLabel() {
        addSubview(dateIcon)
        addSubview(label)
        
        dateIcon.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.height.equalTo(20)
            make.centerY.equalToSuperview()
        }
        
        label.snp.makeConstraints { make in
            make.leading.equalTo(dateIcon.snp.trailing).offset(4)
            make.centerY.equalTo(dateIcon)
        }
    }
    
    @objc fileprivate func tappedLabel(sender: UIButton) {
        if let closure = tappedDate {
            closure()
        }
    }

}
