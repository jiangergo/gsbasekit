//
//  EmptyView.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/25.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    fileprivate lazy var emptyLab: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = UIColor.black
        label.text = "此模块正在努力建设中,敬请期待..."
        return label
    }()
    
    fileprivate lazy var emptyIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(named: "building_icon")
        return icon
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureView() {
        addSubview(emptyIcon)
        addSubview(emptyLab)
        
        emptyIcon.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-18)
        }
        
        emptyLab.snp.makeConstraints { make in
            make.top.equalTo(emptyIcon.snp.bottom).offset(2)
            make.centerX.equalToSuperview()
        }
    }

}
