//
//  UpdateView.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/29.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class UpdateView: UIView {
    
    fileprivate let BaseTag = 900
    
    var update: (() -> Void)?
    var nothing: (() -> Void)?
    
    fileprivate lazy var shadow: UIView = {
        let shadow = UIView()
        shadow.backgroundColor = UIColor.black
        shadow.alpha = 0.7
        return shadow
    }()
    
    fileprivate lazy var backImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "update_back")
        view.isUserInteractionEnabled = true
        return view
    }()
    
    fileprivate lazy var titleLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.text = "版本更新"
        return label
    }()
    
    fileprivate lazy var subTitleLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "Updated version"
        return label
    }()
    
    fileprivate lazy var describeLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "您必须更新才能正常使用!"
        return label
    }()
    
    fileprivate lazy var currentVersion: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "当前版本: "
        return label
    }()
    
    fileprivate lazy var updateVersionLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "最新版本: "
        return label
    }()
    
    lazy var updateVersion: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "1.0.0"
        return label
    }()
    
    fileprivate lazy var currentLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "新版本说明:"
        return label
    }()
    
    lazy var currentDes: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "iOS版本升级说明"
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate lazy var updateBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setBackgroundImage(UIImage(named: "update_yes"), for: .normal)
        btn.setTitle("立即升级", for: . normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.tag = BaseTag
        btn.addTarget(self, action: #selector(tapButtonAction(sender:)), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var noBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setBackgroundImage(UIImage(named: "update_no"), for: .normal)
        btn.setTitle("暂不升级", for: . normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.tag = BaseTag + 1
        btn.addTarget(self, action: #selector(tapButtonAction(sender:)), for: .touchUpInside)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
        
        if let info = Bundle.main.infoDictionary {
            if let version = info["CFBundleShortVersionString"] as? String {
                currentVersion.text = (currentVersion.text ?? "") + version
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        get {
//            return CGSize(width: 233, height: updateBtn.frame.origin.y + updateBtn.intrinsicContentSize.height + 20)
            return CGSize(width: ScreenWidth, height: ScreenHeight)
        }
    }
    
    fileprivate func configureView() {
        //        backgroundColor = UIColor.white
        addSubview(shadow)
        addSubview(backImage)
        
        backImage.addSubview(titleLab)
        backImage.addSubview(subTitleLab)
        backImage.addSubview(describeLab)
        backImage.addSubview(currentVersion)
        backImage.addSubview(updateVersionLab)
        backImage.addSubview(updateVersion)
        backImage.addSubview(currentLab)
        backImage.addSubview(currentDes)
        backImage.addSubview(updateBtn)
        backImage.addSubview(noBtn)
        
        shadow.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(233)
            make.height.equalTo(320)
        }
        
        titleLab.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.leading.equalToSuperview().offset(20)
        }
        
        subTitleLab.snp.makeConstraints { make in
            make.top.equalTo(titleLab.snp.bottom).offset(8)
            make.leading.equalTo(titleLab)
        }
        
        describeLab.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(-20)
            make.leading.equalTo(titleLab)
        }
        
        currentVersion.snp.makeConstraints { make in
            make.leading.equalTo(describeLab).offset(10)
            make.top.equalTo(describeLab.snp.bottom).offset(2)
        }
        
        updateVersionLab.snp.makeConstraints { make in
            make.leading.equalTo(currentVersion)
            make.top.equalTo(currentVersion.snp.bottom).offset(2)
        }
        
        updateVersion.snp.makeConstraints { make in
            make.leading.equalTo(updateVersionLab.snp.trailing)
            make.centerY.equalTo(updateVersionLab)
        }
        
        currentLab.snp.makeConstraints { make in
            make.leading.equalTo(describeLab)
            make.top.equalTo(updateVersion.snp.bottom).offset(2)
        }
        
        currentDes.snp.makeConstraints { make in
            make.leading.equalTo(currentVersion)
            make.top.equalTo(currentLab.snp.bottom).offset(2)
        }
        
        updateBtn.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-20)
            make.top.equalTo(currentDes.snp.bottom).offset(10)
        }
        
        noBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalTo(updateBtn)
            make.width.height.equalTo(updateBtn)
        }
    }
    
    @objc fileprivate func tapButtonAction(sender: UIButton) {
        switch sender.tag - BaseTag {
        case 0:
            if let closure = update {
                closure()
            }
        case 1:
            if let closure = nothing {
                closure()
            }
        default:
            break
        }
    }
    
}
