//
//  TableHeader.swift
//  StarClass_iOS
//
//  Created by Jiangergo on 2018/9/11.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class TableHeader: UIView {
    
    var tappedTaxis: ((_ isDes: Bool) -> ())?
    
    fileprivate lazy var noLab: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "序号"
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var orgName: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "供电所"
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var director: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "所长"
        label.textAlignment = .center
        return label
    }()
    
//    fileprivate lazy var rank: UILabel = {
//        let label = UILabel()
//        label.textColor = UIColor.black
//        label.font = UIFont.systemFont(ofSize: 12)
//        label.text = "排名"
//        label.textAlignment = .center
//        return label
//    }()
    
    fileprivate lazy var rank: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(red: 5.0 / 255.0, green: 160.0 / 255.0, blue: 227.0 / 255.0, alpha: 0.1)
        btn.layer.cornerRadius = 3
        btn.layer.masksToBounds = true
        btn.setImage(UIImage(named: "ascend_icon"), for: .selected)
        btn.setImage(UIImage(named: "descend_icon"), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        btn.setTitle("排名", for: .normal)
        btn.setTitleColor(SouthNetworkBlue, for: .normal)
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -4, 0, 4)
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 2)
        btn.addTarget(self, action: #selector(taxisAction(sender:)), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var score: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "得分"
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var huanbi: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "环比"
        label.textAlignment = .center
        return label
    }()
    
//    lazy var taxis: UIButton = {
//        let btn = UIButton(type: .custom)
//        btn.backgroundColor = UIColor(red: 5.0 / 255.0, green: 160.0 / 255.0, blue: 227.0 / 255.0, alpha: 0.1)
//        btn.layer.cornerRadius = 3
//        btn.layer.masksToBounds = true
//        btn.setImage(UIImage(named: "ascend_icon"), for: .selected)
//        btn.setImage(UIImage(named: "descend_icon"), for: .normal)
//        btn.setTitle("倒序", for: .selected)
//        btn.setTitle("正序", for: .normal)
//        btn.titleLabel?.font = UIFont.systemFont(ofSize: 11)
//        btn.setTitleColor(SouthNetworkBlue, for: .normal)
//        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -4, 0, 4)
//        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 2)
//        btn.addTarget(self, action: #selector(taxisAction(sender:)), for: .touchUpInside)
//        return btn
//    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureHeader()
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: UIViewNoIntrinsicMetric, height: 30)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureHeader() {
        backgroundColor = UIColor.white
        addSubview(noLab)
        addSubview(orgName)
        addSubview(director)
        addSubview(rank)
        addSubview(score)
        addSubview(huanbi)
//        addSubview(taxis)
        
        noLab.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(SmallMargin)
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.1)
        }
        
        let line1 = UIView()
        line1.backgroundColor = SeparatorColor
        addSubview(line1)
        
        line1.snp.makeConstraints { make in
            make.leading.equalTo(noLab.snp.trailing)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.width.equalTo(1)
        }
        
        orgName.snp.makeConstraints { make in
            make.leading.equalTo(noLab.snp.trailing)
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.28)
        }
        
        let line2 = UIView()
        line2.backgroundColor = line1.backgroundColor
        addSubview(line2)
        
        line2.snp.makeConstraints { make in
            make.leading.equalTo(orgName.snp.trailing)
            make.top.bottom.equalTo(line1)
            make.width.equalTo(1)
        }
        
        director.snp.makeConstraints { make in
            make.leading.equalTo(orgName.snp.trailing)
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.15)
        }
        
        let line3 = UIView()
        line3.backgroundColor = line1.backgroundColor
        addSubview(line3)
        
        line3.snp.makeConstraints { make in
            make.leading.equalTo(director.snp.trailing)
            make.top.bottom.equalTo(line1)
            make.width.equalTo(1)
        }
        
        rank.snp.makeConstraints { make in
            make.leading.equalTo(director.snp.trailing).offset(5)
//            make.top.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.width.equalToSuperview().multipliedBy(0.14)
        }
        
        let line4 = UIView()
        line4.backgroundColor = line1.backgroundColor
        addSubview(line4)
        
        line4.snp.makeConstraints { make in
            make.leading.equalTo(rank.snp.trailing).offset(5)
            make.top.bottom.equalTo(line1)
            make.width.equalTo(1)
        }
        
        score.snp.makeConstraints { make in
            make.leading.equalTo(rank.snp.trailing).offset(5)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(huanbi)
        }
        
        let line5 = UIView()
        line5.backgroundColor = line1.backgroundColor
        addSubview(line5)
        
        line5.snp.makeConstraints { make in
            make.leading.equalTo(score.snp.trailing)
            make.top.bottom.equalTo(line1)
            make.width.equalTo(1)
        }
        
        huanbi.snp.makeConstraints { make in
            make.leading.equalTo(score.snp.trailing)
            make.top.bottom.equalToSuperview()
//            make.trailing.equalTo(taxis.snp.leading)
            make.trailing.equalToSuperview().offset(-SmallMargin)
        }
        
//        taxis.snp.makeConstraints { make in
//            make.trailing.equalToSuperview().offset(-SmallMargin)
//            make.height.equalTo(22)
//            make.width.equalTo(54)
//            make.centerY.equalToSuperview()
//        }
        
        let bottomLine = UIView()
        bottomLine.backgroundColor = LineColor
        addSubview(bottomLine)
        
        bottomLine.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
    
    @objc fileprivate func taxisAction(sender: UIButton) {
//        taxis.isSelected = !taxis.isSelected
//        if let closure = tappedTaxis {
//            closure(!taxis.isSelected)
//        }
        
        rank.isSelected = !rank.isSelected
        if let closure = tappedTaxis {
            closure(!rank.isSelected)
        }
    }
}
