//
//  BaseViewController.swift
//  PowerMarket
//
//  Created by Jiangergo on 29/03/2018.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {
    
    private lazy var backgroundImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "background_image")
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar()
        setBackgroundView()
    }
    
    fileprivate func setNavBar() {
        
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.shadowImage = UIImage()
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_setting"), style: .plain, target: self, action: #selector(toSetting(sender:)))
    }
    
    fileprivate func setBackgroundView() {
        view.backgroundColor = UIColor.white
        view.addSubview(backgroundImage)
        
        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc fileprivate func toSetting(sender: UIBarButtonItem) {
        navigationController?.pushViewController(SettingController(), animated: true)
    }

}
