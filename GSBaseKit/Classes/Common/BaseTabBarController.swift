//
//  BaseTabBarController.swift
//  ProductionIndex
//
//  Created by Jiangergo on 2018/6/22.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTabBar()
    }

    fileprivate func configureTabBar() {
        tabBar.tintColor = SouthNetworkBlue
        tabBar.backgroundColor = UIColor.white
        
        addchild(child: StationController(), title: "星级班所", imageName: "tab_icon_station_normal", selectImageName: "tab_icon_station_selected")
        addchild(child: TeamController(), title: "班组", imageName: "tab_icon_team_normal", selectImageName: "tab_icon_team_selected")
    }
    
    fileprivate func addchild(child: UIViewController, title: String, imageName: String, selectImageName: String) {
        let item = UITabBarItem(title: title, image: UIImage(named: imageName), selectedImage: UIImage(named: selectImageName))
        child.tabBarItem = item
        child.title = title
        
        let nav = BaseNavigationController(rootViewController: child)
        addChildViewController(nav)
    }

}
