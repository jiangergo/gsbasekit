//
//  Protocol.swift
//  StarClass_iOS
//
//  Created by Jiangergo on 2018/9/11.
//  Copyright © 2018 gzgs. All rights reserved.
//

import Foundation

protocol Nibloadable {
    
}

extension Nibloadable where Self : UIView {
    
     static func loadNib(_ nibName :String = "") -> Self{
     let nib = nibName == "" ? "\(self)" : nibName
     return Bundle.main.loadNibNamed(nib, owner: nil, options: nil)?.first as! Self
     }
 
//    static func loadNib(_ nibNmae :String? = nil) -> Self {
//        return Bundle.main.loadNibNamed(nibNmae ?? "\(self)", owner: nil, options: nil)?.first as! Self
//    }
}
