//
//  Extension.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/25.
//  Copyright © 2018 gzgs. All rights reserved.
//

import Foundation

extension String {
    
    func urlEncoded() -> String {
        let encodeUrlString = self.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) ?? ""
        return encodeUrlString
    }
    
    func urlGBKEnCoded() -> String {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        let url = String(data: GTMBase64.encode(data), encoding: String.Encoding.utf8)
        return url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
    static func getDayDate(date: Date = Date()) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy年MM月dd日"
        let dateStr = date
        return formatter.string(from: dateStr)
    }
    
    static func getMonthDate(date: Date = Date()) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy年MM月"
        let dateStr = date
        return formatter.string(from: dateStr)
    }
    
    static func getFormatterDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    static func getFormatterMonthDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM"
        return formatter.string(from: date)
    }
    
    static func getDateForDateComponents(dateComponents: DateComponents) -> Date {
        let year = "\(dateComponents.year ?? 2018)"
        let month = "\(dateComponents.month ?? 01)"
        let day = "\(dateComponents.day ?? 01)"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: year + "-" + month + "-" + day) ?? Date()
    }
    
    static func getMonthForDateComponents(dateComponents: DateComponents) -> Date {
        let year = "\(dateComponents.year ?? 2018)"
        let month = "\(dateComponents.month ?? 01)"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM"
        return formatter.date(from: year + "-" + month) ?? Date()
    }
    
    static func getPriousorLaterMonth(month: Int) -> Date {
        var components = DateComponents()
        components.month = month
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        return cal.date(byAdding: components, to: Date()) ?? Date()
    }
    
    static func getUUID() -> String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    static func getDeviceId() -> String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    //MARK: - MD5
    func md5() -> String {
        let cStr = self.cString(using: String.Encoding.utf8);
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 16)
        CC_MD5(cStr!,(CC_LONG)(strlen(cStr!)), buffer)
        let md5String = NSMutableString();
        for i in 0 ..< 16{
            md5String.appendFormat("%02x", buffer[i])
        }
        free(buffer)
        return md5String.uppercased as String
    }
    
    static func getFormValue(str: String) -> String {
        let nsStr = str as NSString
        
        let start = nsStr.range(of: "<ns1:out>")
        let end = nsStr.range(of: "</ns1:out>")
        
        //        debugPrint(nsStr)
        guard nsStr.contains("<ns1:out>") else {
            return ""
        }
        let result = nsStr.substring(with: NSMakeRange(start.location + start.length, end.location - start.location - start.length))
        //        debugPrint(result)
        let data = result.data(using: String.Encoding.utf8)!
        var dict: NSDictionary = [:]
        
        do {
            dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        }
        catch {
            print("解析失败")
        }
        
        return dict["value"] as? String ?? ""
        
    }
    
    func getDataFromJSONString() -> [[String: Any]] {
        let jsonData:Data = self.data(using: .utf8)!
        
        let data = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if let arr = data as? NSArray {
            return arr as! [[String: Any]]
        }
        
        if let dict = data as? NSDictionary {
            if let data = dict["data"] {
                return data as! [[String: Any]]
            }
            else {
                return [dict as! Dictionary<String, Any>]
            }
        }
        return []
    }
    
    func getArrayFromJSONString() -> NSArray {
        
        let jsonData:Data = self.data(using: .utf8)!
        
        let arr = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if let data = arr {
            return data as! NSArray
        }
//                if dict != nil {
//
//                    return dict as! NSDictionary
//                }
        return NSArray()
    }
    
    func getDictionaryFromJSONString() -> NSDictionary {
        
        let jsonData:Data = self.data(using: .utf8)!
        
        let dict = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if let dic = dict {
            return dic as! NSDictionary
        }
//        if dict != nil {
//            
//            return dict as! NSDictionary
//        }
        return NSDictionary()
    }
}

extension Date {
    var milliStamp : String {
        let timeInterval: TimeInterval = self.timeIntervalSince1970
        let millisecond = CLongLong(round(timeInterval*1000))
        return "\(millisecond)"
    }
}

extension UIImage {
    static func creatImageWithColor(color:UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension CAGradientLayer {
    
    //获取彩虹渐变层
    func rainbowLayer() -> CAGradientLayer {
        //定义渐变的颜色
        let gradientColors = [UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.00).cgColor,
                              UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.00).cgColor]
        
        //定义每种颜色所在的位置
        let gradientLocations:[NSNumber] = [0.0, 0.8]
        
        //创建CAGradientLayer对象并设置参数
        self.colors = gradientColors
        self.locations = gradientLocations
        
        //设置渲染的起始结束位置（横向渐变）
        self.startPoint = CGPoint(x: 0, y: 0)
        self.endPoint = CGPoint(x: 1, y: 0)
        
        return self
    }
}

extension UIColor{
    class func rgbColorFromHex(rgb: UInt64) -> UIColor {
        return UIColor(red: ((CGFloat)((rgb & 0xFF0000) >> 16)) / 255.0,
                       green: ((CGFloat)((rgb & 0xFF00) >> 8)) / 255.0,
                       blue: ((CGFloat)(rgb & 0xFF)) / 255.0,
                       alpha: 1.0)
    }
}
