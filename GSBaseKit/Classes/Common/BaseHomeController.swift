//
//  BaseHomeController.swift
//  ProdQuota_iOS
//
//  Created by Jiangergo on 2018/6/26.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

class BaseHomeController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_home"), style: .plain, target: self, action: #selector(toHome(sender:)))
    }
    
    
    @objc fileprivate func toHome(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

}
