//
//  Theme.swift
//  PowerMarket
//
//  Created by Jiangergo on 29/03/2018.
//  Copyright © 2018 gzgs. All rights reserved.
//

import UIKit

// MARK: - 全局常用属性
public let ScreenWidth: CGFloat = UIScreen.main.bounds.size.width
public let ScreenHeight: CGFloat = UIScreen.main.bounds.size.height
public let ScreenBounds: CGRect = UIScreen.main.bounds
public let NavigationH: CGFloat = ScreenHeight == 812 ? 88 : 64
public let CommonMargin: CGFloat = ScreenWidth <= 375.0 ? 12 : 15
public let HomeMargin: CGFloat = ScreenWidth <= 375.0 ? 12 : 15
public let MiddleMargin: CGFloat = ScreenWidth <= 375.0 ? 10 : 12
public let SubMargin: CGFloat = ScreenWidth <= 375.0 ? 7 : 10
public let SmallMargin: CGFloat = ScreenWidth <= 375.0 ? 3 : 5
public let TabBarH: CGFloat = 49
public let NavBarMargin: CGFloat = ScreenWidth <= 375.0 ? 12 : 20

public let ScreenScale: CGFloat = UIScreen.main.scale
public let GrayBackgroundColor: UIColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
public let GrayHeaderColor: UIColor = UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.00)
public let ButtonBorder: UIColor = UIColor(red:0.27, green:0.44, blue:0.61, alpha:1.00)
public let GrayLabelColor: UIColor = UIColor(red:0.80, green:0.81, blue:0.80, alpha:1.00)
public let LightBlue: UIColor = UIColor(red:0.00, green:0.64, blue:0.88, alpha:1.00)
public let SouthNetworkBlue: UIColor = UIColor(red:0.00, green:0.34, blue:0.72, alpha:1.00)
public let SkyBlue: UIColor = UIColor(red:0.13, green:0.49, blue:0.96, alpha:1.00)
public let LineColor: UIColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:0.80)
public let SeparatorColor: UIColor = UIColor(red:0.45, green:0.45, blue:0.45, alpha:1.00)
public let ProductionBlue: UIColor = UIColor(red: 22.0 / 255.0, green: 155.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)

public let CoursePurple: UIColor = UIColor(red: 165.0 / 255.0, green: 109.0 / 255.0, blue: 254.0 / 255.0, alpha: 1.0)
public let CellColor: UIColor = UIColor(red:0.00, green:0.65, blue:0.87, alpha:1.00)
public let SegmentBackColor: UIColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.00)
public let ContentGreyColor: UIColor = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
public let TagColor: UIColor = UIColor(red:0.00, green:0.41, blue:0.73, alpha:1.00)

public let percentFont: UIFont = ScreenWidth <= 375.0 ? UIFont.boldSystemFont(ofSize: 14) : UIFont.boldSystemFont(ofSize: 16)
public let productionNumberFont: UIFont = ScreenWidth <= 375.0 ? UIFont.boldSystemFont(ofSize: 26) : UIFont.boldSystemFont(ofSize: 32)
